
------------------------------------------------------

	AP Ranking
1	alabama
2	clemson
3	pennsylvania st
4	georgia
5	washington
6	texas christian
7	wisconsin
8	washington st
9	ohio st
10	auburn
11	miami
12	oklahoma
13	southern cal
14	oklahoma st
15	virginia tech
16	notre dame
17	michigan
18	south florida
19	san diego st
20	north carolina st
21	michigan st
22	central florida
23	stanford
24	texas tech
25	navy

------------------------------------------------------

	User Ranking
1	alabama
2	clemson
3	pennsylvania st
4	georgia
5	washington
6	texas christian
7	wisconsin
8	oklahoma
9	ohio st
10	washington st
11	auburn
12	miami
13	southern cal
14	michigan
15	oklahoma st
16	virginia tech
17	south florida
18	san diego st
19	notre dame
20	north carolina st
21	louisville
22	central florida
23	georgia tech
24	stanford
25	texas tech

------------------------------------------------------

Teams user included in rankings that AP did not: 
louisville
georgia tech

------------------------------------------------------

Team AP included in rankings that user did not: 
michigan st
navy

------------------------------------------------------

Number of teams in exact position:     11
Number of teams that user had correct: 23

------------------------------------------------------

EDIT Distance:  10
HAMM Distance:  14
LCS  Distance:  12

------------------------------------------------------

AVG  Distance:  12
