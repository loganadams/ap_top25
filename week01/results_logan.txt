
------------------------------------------------------

	AP Ranking
1	alabama
2	ohio st
3	clemson
4	pennsylvania st
5	oklahoma
6	southern cal
7	washington
8	michigan
9	wisconsin
10	florida st
11	oklahoma st
12	louisiana st
13	auburn
14	stanford
15	georgia
16	miami
17	louisville
18	virginia tech
19	kansas st
20	washington st
21	south florida
22	florida
23	texas christian
24	notre dame
25	tennessee

------------------------------------------------------

	User Ranking
1	alabama
2	ohio st
3	clemson
4	pennsylvania st
5	oklahoma
6	southern cal
7	washington
8	michigan
9	florida st
10	wisconsin
11	oklahoma st
12	auburn
13	stanford
14	louisiana st
15	georgia
16	miami
17	louisville
18	south florida
19	kansas st
20	virginia tech
21	florida
22	tennessee
23	washington st
24	texas christian
25	west virginia

------------------------------------------------------

Teams user included in rankings that AP did not: 
west virginia

------------------------------------------------------

Team AP included in rankings that user did not: 
notre dame

------------------------------------------------------

Number of teams in exact position:     13
Number of teams that user had correct: 24

------------------------------------------------------

EDIT Distance:  11
HAMM Distance:  12
LCS  Distance:  14

------------------------------------------------------

AVG  Distance:  11
