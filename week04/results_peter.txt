
------------------------------------------------------

	AP Ranking
1	alabama
2	clemson
3	oklahoma
4	pennsylvania st
5	southern cal
6	washington
7	georgia
8	michigan
9	texas christian
10	wisconsin
11	ohio st
12	virginia tech
13	auburn
14	miami
15	oklahoma st
16	washington st
17	louisville
18	south florida
19	san diego st
20	utah
21	florida
22	notre dame
23	west virginia
24	mississippi st
25	louisiana st

------------------------------------------------------

	User Ranking
1	alabama
2	clemson
3	oklahoma
4	pennsylvania st
5	washington
6	southern cal
7	michigan
8	oklahoma st
9	wisconsin
10	ohio st
11	georgia
12	texas christian
13	virginia tech
14	miami
15	auburn
16	washington st
17	louisville
18	south florida
19	florida
20	mississippi st
21	san diego st
22	utah
23	louisiana st
24	west virginia
25	notre dame

------------------------------------------------------

Teams user included in rankings that AP did not: 

------------------------------------------------------

Team AP included in rankings that user did not: 

------------------------------------------------------

Number of teams in exact position:     8
Number of teams that user had correct: 25

------------------------------------------------------

EDIT Distance:  13
HAMM Distance:  17
LCS  Distance:  18

------------------------------------------------------

AVG  Distance:  15
