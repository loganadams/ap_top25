#!/usr/bin/env python

import sys
import numpy as np
import os
from multiprocessing import Pool

# Enable multithreading and pooling
# os.system("taskset -p 0xff %d" % os.getpid())

## HAMMING DISTANCE
def hammingDistance(x, y):
    ''' Return Hamming distance between x and y '''
    assert len(x) == len(y)
    nmm = 0
    for i in range(0, len(x)):
        if x[i] != y[i]:
            nmm += 1
    return nmm 

## EDIT DISTANCE
def editDistance(x, y):
    ''' Calculate edit distance between sequences x and y using matrix dynamic programming.  Return distance. '''
    D = np.zeros((len(x) + 1, len(y) + 1), dtype=int)
    D[0, 1:] = range(1, len(y) + 1)
    D[1:, 0] = range(1, len(x) + 1)
    for i in range(1, len(x) + 1):
        for j in range(1, len(y) + 1):
            delt = 1 if x[i - 1] != y[j - 1] else 0
            D[i, j] = min(D[i - 1, j - 1] + delt, D[i - 1, j] + 1, D[i, j - 1] + 1)
    return D[len(x), len(y)]

## LCS DISTANCE - LONGEST COMMON SUBSEQUENCE
def lcsDistance(x, y):
    ''' Calculate edit distance between sequences x and y using matrix dynamic programming.  Return distance. '''
    D = np.zeros((len(x) + 1, len(y) + 1), dtype=int)
    D[0, 1:] = range(1, len(y) + 1)
    D[1:, 0] = range(1, len(x) + 1)
    for i in range(1, len(x) + 1):
        for j in range(1, len(y) + 1):
            delt = 2 if x[i - 1] != y[j - 1] else 0
            D[i, j] = min(D[i - 1, j - 1] + delt, D[i - 1, j] + 1, D[i, j - 1] + 1)
    return D[len(x), len(y)]


## MAIN

# Always enter AP rankings first
file_ap = open(sys.argv[1], "r")
rankings_ap = file_ap.readlines()
file_ap.close()

# Always enter user rankings second
file_user = open(sys.argv[2], "r")
rankings_user = file_user.readlines()
file_user.close()

print
print "------------------------------------------------------"
print

# Print the rankings to ensure that the correct files are read
print("\tAP Ranking")
for team in range(0, 25):
    print str(team+1) + "\t" + str(rankings_ap[team].strip())
    
print
print "------------------------------------------------------"
print

print("\tUser Ranking")
for team in range(0, 25):
    print str(team+1) + "\t" + str(rankings_user[team].strip())

# Modify the arrays to perform correct Distance calculations
rankings_ap_trim = [team.strip() for team in rankings_ap]
rankings_user_trim = [team.strip() for team in rankings_user]
   
# Calculate the Hamming and LCS and Edit Distances
d_edit = editDistance(rankings_ap_trim, rankings_user_trim)
d_lcs  = lcsDistance(rankings_ap_trim, rankings_user_trim)
d_hamm = hammingDistance(rankings_ap_trim, rankings_user_trim)

print
print "------------------------------------------------------"
print

user_wrong = 0
ap_wrong = 0

print "Teams user included in rankings that AP did not: "
for wrong in rankings_user_trim:
    if wrong not in rankings_ap_trim:
        print wrong
        ap_wrong = ap_wrong + 1
    if not wrong:
        print "none"

print
print "------------------------------------------------------"
print
    
print "Team AP included in rankings that user did not: "
for wrong in rankings_ap_trim:
    if wrong not in rankings_user_trim:
        print wrong
        user_wrong = user_wrong + 1
    if not wrong:
        print "none"
    
if user_wrong != ap_wrong:
    sys.exit('ERROR')

print
print "------------------------------------------------------"
print


print "Number of teams in exact position:     " + str(25 - d_hamm)
print "Number of teams that user had correct: " + str(25 - user_wrong)

print
print "------------------------------------------------------"
print


print "EDIT Distance:  " + str(d_edit)
print "HAMM Distance:  " + str(d_hamm)
print "LCS  Distance:  " + str(d_lcs)

print
print "------------------------------------------------------"
print

print "AVG  Distance:  " + str((d_edit + d_hamm) / 2)

