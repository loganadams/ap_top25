
------------------------------------------------------

	AP Ranking
1	alabama
2	clemson
3	oklahoma
4	pennsylvania st
5	georgia
6	washington
7	michigan
8	texas christian
9	wisconsin
10	ohio st
11	washington st
12	auburn
13	miami
14	southern cal
15	oklahoma st
16	virginia tech
17	louisville
18	south florida
19	san diego st
20	utah
21	notre dame
22	florida
23	west virginia
24	north carolina st
25	central florida

------------------------------------------------------

	User Ranking
1	alabama
2	clemson
3	oklahoma
4	pennsylvania st
5	georgia
6	washington
7	michigan
8	texas christian
9	washington st
10	wisconsin
11	ohio st
12	auburn
13	miami
14	oklahoma st
15	southern cal
16	louisville
17	south florida
18	san diego st
19	virginia tech
20	utah
21	florida
22	notre dame
23	west virginia
24	north carolina st
25	oregon

------------------------------------------------------

Teams user included in rankings that AP did not: 
oregon

------------------------------------------------------

Team AP included in rankings that user did not: 
central florida

------------------------------------------------------

Number of teams in exact position:     13
Number of teams that user had correct: 24

------------------------------------------------------

EDIT Distance:  8
HAMM Distance:  12
LCS  Distance:  10

------------------------------------------------------

AVG  Distance:  10
